 <!-- <div class="loading-animation-screen">
    <div class="overlay-screen"></div>
    <img src="<?= base_url(); ?>assets/images/icon/loading.gif" alt="loading.." class="img-loading">
  </div> -->

  <?php
  $setting = $this->db->get('settings')->row_array();
  $dateNow = date('Y-m-d H:i');
  $dateDB = $setting['promo_time'];
  $dateDBNew = str_replace("T", " ", $dateDB);
  if ($dateNow >= $dateDBNew) {
    $this->db->set('promo', 0);
    $this->db->update('settings');
  }
  ?>
<body>
	<?php
	$settingss = $this->db->get('settings')->row_array();
	?>
    <nav class="navbar navbar-expand-lg navbar-light navbar-store fixed-top navbar-fixed-top" data-aos="fade-down" aria-label="">
      <div class="container">
        <a href="<?= base_url(); ?>" class="navbar-brand">
          <img src="<?= base_url(); ?>assets/images/logo/<?= $settingss['logo']; ?>" width="50px" height="50px" alt="logo" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="<?= base_url(); ?>" class="nav-link">Beranda</a>
            </li>
            <li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Kategori
							</a>
							 <?php $categories = $this->Categories_model->getCategories(); ?>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<?php foreach ($categories->result_array() as $cat) : ?>
								<a class="dropdown-item" href="<?= base_url(); ?>c/<?= $cat['slug']; ?>"><?= $cat['name']; ?></a>
								<?php endforeach; ?>
							</div>
						</li>
            <li class="nav-item">
              <a href="<?= base_url(); ?>products" class="nav-link">Semua Produk</a>
            </li>
          </ul>
           <!-- dekstop -->
           <ul class="navbar-nav d-none d-lg-flex">
              <li>
                <li class="nav-item">
                  <a href="<?= base_url(); ?>cart" class="nav-link d-inline-block navbar-cart-inform mt-2">
										<?php if ($this->cart->total_items() > 0) { ?>
										<img src="<?= base_url();  ?>assets/images/icon-cart-filled.svg" alt="">
                    <div class="card-badge"><?= count($this->cart->contents()); ?></div>
										<?php } else { ?>
										<img src="<?= base_url();  ?>assets/images/icon-cart-empty.svg" alt="">
										<?php } ?>
                  </a>
                </li>
              </li>
            </ul>
            <!-- mobile -->
          <ul class="navbar-nav d-blok d-lg-none">
             <li class="nav-item">
              <a href="<?= base_url(); ?>cart" class="nav-link d-inline-block">
                Chart
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
