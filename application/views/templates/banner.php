 <!-- page content -->
    <div class="page-content page-home">
     <!-- banner -->
		 <?php $banner = $this->Settings_model->getBanner(); ?>
		  <section class="store-carousel">
        <div class="container">
          <div class="row">
            <div class="col-lg-12" data-aos="zoom-in">
              <div
                id="storeCarousel"
                class="carousel slide"
                data-ride="carousel"
              >
                <ol class="carousel-indicators">
				<?php
					foreach ($banner->result_array() as $key => $value) {
					$active = ($key == 0) ? 'active' : '';
					echo '<li data-target="#storeCarousel" data-slide-to="' . $key . '" class="{' . $active . '"></li>';
				}
				?>
                <?php foreach($banners as $banner) : ?>
                    <li data-target="#storeCarousel" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                <?php endforeach; ?>
                </ol>

                <div class="carousel-inner rounded-lg">
					<?php
					foreach ($banner->result_array() as $key => $value) {
							$active = ($key == 0) ? 'active' : '';
							echo '<div class="carousel-item ' . $active . '">
									<a href="'.$value['url'].'"><img src="' . base_url() . 'assets/images/banner/' . $value['img'] . '"></a>
										</div>';
					}
					?>
                </div>

              </div>
				<a class="carousel-control-prev" href="#storeCarousel" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#storeCarousel" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
            </div>
          </div>
        </div>
      </section>
