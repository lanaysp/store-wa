 <!-- content -->
    <div class="page-content page-cart">
      <section class="store-breadcrumbs" data-aos="fade-down" data-aos-delay="100">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <nav aria-label="">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="<?= base_url(); ?>">Home</a>
                  </li>
                  <li class="breadcrumb-item">
                    <a href="<?= base_url(); ?>cart">Troli</a>
                  </li>
                   <li class="breadcrumb-item active">
                    Pembayaran
                  </li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </section>

      <section class="store-cart">
        <div class="container">
         <div class="detail-produk" data-aos="fade-up" data-aos-delay="150">
					 <div class="col-12">
              <h2 class="mb-4">
                Detail Pembelian
              </h2>
            </div>
					 <table class="table table-striped table-responsive">
                <tr>
                    <th>Produk</th>
                    <th>Jumlah</th>
                    <th>Ket</th>
                    <th>Harga</th>
                </tr>
                <?php foreach($this->cart->contents() as $item): ?>
                <tr>
                    <td># <?= $item['name']; ?></td>
                    <td class="text-center"><?= $item['qty']; ?></td>
                    <?php if($item['ket'] == ""){ ?>
                        <td>-</td>
                    <?php }else{ ?>
                        <td><?= $item['ket']; ?></td>
                    <?php } ?>
                    <td>Rp<?= number_format($item['subtotal'],0,",","."); ?></td>
                </tr>
                <?php endforeach; ?>
            </table>
					
				 </div>
          <div class="row" data-aos="fade-up" data-aos-delay="150">
            <div class="col-12">
              <hr/>
            </div>
            <div class="col-12">
              <h2 class="mb-4">
                Alamat Pengiriman
              </h2>
            </div>
          </div>
					
		  <form action="<?= base_url(); ?>payment/succesfully" method="post">
          <div class="row mb-2" data-aos="fade-up" data-aos-delay="200">
			  <?php if($this->cart->total_items() > 0){ ?>
              <div class="col-md-6">
                <div class="form-group">
					<label for="name">Nama</label>
					<input type="text" id="name" autocomplete="off" class="form-control" required name="name">
				</div>
              </div>
              <div class="col-md-6">
                 <div class="form-group">
					<label for="telp">Nomor Telepon</label>
					<input type="number" id="telp" autocomplete="off" class="form-control" required name="telp">
					<small class="text-muted">Contoh: 081234567890</small>
				</div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
					<label for="paymentSelectProvinces">Provinsi</label>
					<select name="paymentSelectProvinces" id="paymentSelectProvinces" class="form-control" required>
						<option></option>
						<?php foreach($provinces as $p): ?>
							<option value="<?= $p['province_id']; ?>"><?= $p['province']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
              </div>
              <div class="col-md-4">
                <?php if($setting['ongkir'] == 0){ ?>
                <div class="form-group">
                    <label for="paymentSelectRegenciesOngkir">Kabupaten/Kota</label>
                    <select name="paymentSelectRegenciesOngkir" id="paymentSelectRegenciesOngkir" class="form-control" required>
                        <option></option>
                    </select>
                </div>
            <?php }else{ ?>
                <div class="form-group">
                    <label for="paymentSelectRegencies">Kabupaten/Kota</label>
                    <select name="paymentSelectRegencies" id="paymentSelectRegencies" class="form-control" required>
                        <option></option>
                    </select>
                </div>
            <?php } ?>
			  </div>
              <div class="col-md-4">
                <div class="form-group">
					<label for="zipcode">Kode Pos</label>
					<input type="number" id="zipcode" autocomplete="off" class="form-control" required name="zipcode">
				</div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
					<label for="district">Kecamatan</label>
					<input type="text" class="form-control" autocomplete="off" id="district" name="district" required>
				</div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
					<label for="village">Desa/Kelurahan</label>
					<input type="text" class="form-control" autocomplete="off" id="village" name="village" required>
				</div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
					<label for="address">Alamat</label>
					<textarea name="address" rows="3" id="address" class="form-control" placeholder="Isi dengan nama jalan, nomor rumah, nama gedung, dsb" required></textarea>
				</div>
              </div>
			  <div class="col-md-6 offset-6">
				  <?php if($setting['ongkir'] != 0){ ?>
					<div class="line mt-4"></div>
					<div class="send">
						<h2 class="title">Metode Pengiriman</h2>
						<small class="text-danger" id="paymentTextNotSupportDelivery" style="display: none;">Metode antar belum tersedia untuk tempat Anda.</small>
						<div class="form-group mt-3" id="groupPaymentSelectKurir">
							<select name="paymentSelectKurir" id="paymentSelectKurir" class="form-control" required>
								<option></option>
							</select>
						</div>
					</div>
					<?php } ?>
			  </div>
			 
			<?php }else{ ?>
				<div class="alert alert-warning col-12">Upss. Kamu belum memiliki satupun belanjaan. Yuk belanja dulu.</div>
			<?php } ?>
          </div>
          <div class="row" data-aos="fade-up" data-aos-delay="150">
            <div class="col-12">
              <hr/>
            </div>
            <div class="col-12">
              <h2>
                Ringkasan Belanja
              </h2>
            </div>
          </div>
          <div class="row" data-aos="fade-up" data-aos-delay="200">
            <div class="col-4 col-md-2">
              <div class="product-title">Rp<?= number_format($this->cart->total(),0,",","."); ?></div>
              <div class="product-subtitle">Total Belanja</div>
            </div>

			<?php if($setting['ongkir'] == 0){ ?>

				<div class="col-4 col-md-2">
					<div class="product-title" id="paymentSendingPriceOngkir">Rp0</div>
              		<div class="product-subtitle">Biaya Pengiriman</div>
				</div>

				<div class="col-4 col-md-2">
					<div class="product-title text-success" id="paymentTotalAll">Rp<?= number_format($setting['default_ongkir'] + $this->cart->total(),0,",","."); ?></div>
					<div class="product-subtitle">Total Tagihan</div>
				</div>

			<?php }else{ ?>
            
            <div class="col-4 col-md-2">
              <div class="product-title" id="paymentSendingPrice">Rp0</div>
              <div class="product-subtitle" >Biaya Pengiriman</div>
            </div>
            <div class="col-4 col-md-2">
              <div class="product-title text-success" id="paymentTotalAll">Rp<?= number_format($this->cart->total(),0,",","."); ?></div>
              <div class="product-subtitle">Total Tagihan</div>
            </div>

			<?php } ?>

			<?php if($this->cart->total_items() > 0){ ?>
            <div class="col-8 col-md-3">
              <button id="btnPaymentNow" type="submit" class="btn btn-success mt-4 btn-block">
              Lanjutkan
              </button>
            </div>
			<?php }else{ ?>
				<div class="col-8 col-md-3">
				  <div class="alert mt-2 alert-warning">Keranjangmu masih kosong.</div>
					<a href="<?= base_url(); ?>" class="btn btn-success mt-4 btn-block">
						Belanja Dulu
					</a>
				  </div>
				<?php } ?>
          </div>
		  </form>
        </div>
      </section>
    </div>
  <!-- end content -->
