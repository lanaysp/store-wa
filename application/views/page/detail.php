 <!-- content -->
    <div class="page-content page-details">
      <section class="store-breadcrumbs" data-aos="fade-down" data-aos-delay="100">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <nav aria-label="">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="<?= base_url(); ?>">Home</a>
                  </li>
                   <li class="breadcrumb-item active">
                   <a href="<?= base_url(); ?>c/<?= $product['slug']; ?>"><?= $product['name']; ?></a>
                  </li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </section>
	 	<?php $setting = $this->db->get('settings')->row_array(); ?>
      <section class="store-gallery" id="img">
        <div class="container">
          <div class="row img">
            <div class="col-lg-8" data-aos="zoom-in">
              <transition name="slide-fade" mode="out-in" >
                <a href="<?= base_url(); ?>assets/images/product/<?= $product['img']; ?>" data-lightbox="img-1">
					<img src="<?= base_url(); ?>assets/images/product/<?= $product['img']; ?>" class="w-100 main-image jumbo-thumb" alt="">
				</a>
              </transition>
            </div>
            <div class="col-lg-2">
              <div class="row img-slider">
                <div class="col-3 col-lg-12 mt-2 mt-lg-1" data-aos="zoom-in" data-aos-delay="100">
                  
				  	<?php foreach($img->result_array() as $d): ?>
                    <img
                      src="<?= base_url(); ?>assets/images/product/<?= $d['img']; ?>"
                      class="w-100 thumbnail-image thumb"
                      alt=""
                    />
					<?php endforeach; ?>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div class="store-detail-container" data-aos="fade-up">
        <section class="store-heading">
          <div class="container">
            <div class="row">
              <div class="col-lg-8">
                <h1><?= $product['title']; ?></h1>
				<div class="row">
					<?php if($setting['promo'] == 1){ ?>
					<?php if($product['promo_price'] == 0){ ?>
					<div class="price">Rp <?= str_replace(",",".",number_format($product['price'])); ?></div>
					<?php }else{ ?>
					<div class="price oldPrice mr-3">Rp <?= str_replace(",",".",number_format($product['price'])); ?></div>
					<div class="price newPrice">Rp <?= str_replace(",",".",number_format($product['promo_price'])); ?></div>
					<?php } ?>
					<?php }else{ ?>
					<div class="price newPrice">Rp <?= str_replace(",",".",number_format($product['price'])); ?></div>
					<?php } ?>
				</div>
                <div class="owner">
					Terjual <?= $product['transaction']; ?> Produk &bull; <?= $product['viewer']; ?>x Dilihat <br>
					Kondisi :
					<?php if($product['condit'] == 1){ ?>
						Baru
					<?php }else{ ?>
						Bekas
					<?php } ?> <br>
					Berat : <?= $product['weight']; ?> gram <br>
					Stok : <?= $product['stock']; ?> produk <br>
					<?php if($product['stock'] > 0){ ?>
                    <tr>
                        <?php if($setting['promo'] == 1){ ?>
                        <?php if($product['promo_price'] == 0){ ?>
                            <?php $priceP = $product['price']; ?>
                        <?php }else{ ?>
                            <?php $priceP = $product['promo_price']; ?>
                        <?php } ?>
                        <?php }else{ ?>
                            <?php $priceP = $product['price']; ?>
                        <?php } ?>
                        Jumlah
                        
						 <button onclick="minusProduct(<?= $priceP; ?>)">-</button><!--
					--><input disabled type="text" value="1" id="qtyProduct" class="valueJml "><!--
					--><button onclick="plusProduct(<?= $priceP; ?>, <?= $product['stock']; ?>)">+</button>
                     
                   <br>
                        Total
                        Rp <span id="detailTotalPrice"><?= str_replace(",",".",number_format($priceP)); ?></span>
                    
                    <?php } ?>
					</tr>
				</div>
				
				
              </div>
              <div class="col-lg-2" data-aos="zoom-in">
				  <?php if($product['stock'] > 0){ ?>
                <button class="btn btn-success px-4 text-white btn-block mb-3" onclick="buy()">
                  Beli
                </button>
                <button class="btn btn-primary px-4 text-white btn-block mb-3" onclick="addCart()">Tambah ke Keranjang</button>
				<?php }else{ ?>
					<p class="btn btn-outline-secondary">Stok lagi kosong</p>
                <?php } ?>
              </div>
            </div>
          </div>
        </section>
        <section class="store-description">
          <div class="container">
            <div class="row">
              <div class="col-12 col-lg-8">
                <p>
                 <?= nl2br($product['description']); ?>
                </p>
              </div>
            </div>
          </div>
        </section>
       
      
      </div>

    </div>
  <!-- end content -->
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script>
		function plusProduct(price, stock){
			let inputJml;
			inputJml = parseInt($("input.valueJml").val());
			inputJml = inputJml + 1;
			if(inputJml <= stock){
				$("input.valueJml").val(inputJml);
				const newPrice = inputJml * price;
				const rpFormat = number_format(newPrice);
				$("#detailTotalPrice").text(rpFormat.split(",").join("."));
			}
		}

		function minusProduct(price){
			let inputJml;
			inputJml = parseInt($("input.valueJml").val());
			inputJml = inputJml - 1;
			if(inputJml >= 1){
				$("input.valueJml").val(inputJml);
				const newPrice = inputJml * price;
				const rpFormat = number_format(newPrice);
				$("#detailTotalPrice").text(rpFormat.split(",").join("."));
			}
		}

		function number_format (number, decimals, decPoint, thousandsSep) {
			number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
			var n = !isFinite(+number) ? 0 : +number
			var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
			var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
			var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
			var s = ''

			var toFixedFix = function (n, prec) {
			var k = Math.pow(10, prec)
			return '' + (Math.round(n * k) / k)
				.toFixed(prec)
			}

			// @todo: for IE parseFloat(0.55).toFixed(0) = 0;
			s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
			if (s[0].length > 3) {
			s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
			}
			if ((s[1] || '').length < prec) {
			s[1] = s[1] || ''
			s[1] += new Array(prec - s[1].length + 1).join('0')
			}

			return s.join(dec)
		}

		function buy(){
			$.ajax({
				url: "<?= base_url(); ?>cart/add_to_cart",
				type: "post",
				data: {
					id: <?= $product['productId']; ?>,
					qty: $("#qtyProduct").val()
				},
				success: function(data){
					location.href = "<?= base_url(); ?>cart"
				}
			})
		}

		function addCart(){
			$.ajax({
				url: "<?= base_url(); ?>cart/add_to_cart",
				type: "post",
				data: {
					id: <?= $product['productId']; ?>,
					qty: $("#qtyProduct").val()
				},
				success: function(data){
					$(".navbar-cart-inform").html(`<img src="<?= base_url();  ?>assets/images/icon-cart-filled.svg" alt=""><div class="card-badge"><?= count($this->cart->contents()) + 1; ?></div>`);
					swal({
						title: "Berhasil Ditambah ke Keranjang",
						text: `<?= $product['title']; ?>`,
						icon: "success",
						buttons: true,
						buttons: ["Lanjut Belanja", "Lihat Keranjang"],
						})
						.then((cart) => {
						if (cart) {
							location.href = "<?= base_url(); ?>cart"
						}
					});
				}
			})
		}

		// slider product
		const containerImgProduct = document.querySelector("div.page-details div.store-gallery div.img");
		const jumboImgProduct = document.querySelector("div.page-details div.store-gallery div.img img.jumbo-thumb");
		const jumboHrefImgProduct = document.querySelector("div.page-details div.store-gallery div.img a");
		const thumbsImgProduct = document.querySelectorAll("div.page-details div.store-gallery div.img div.img-slider img.thumb");
		
		containerImgProduct.addEventListener('click', function(e){
			if(e.target.className == 'thumb'){
				jumboImgProduct.src = e.target.src;
				jumboHrefImgProduct.href = e.target.src;
				
				thumbsImgProduct.forEach(function(thumb){
					thumb.className = 'thumb';
				})
			}
		})

		

	</script>
