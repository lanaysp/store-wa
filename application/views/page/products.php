
    <div class="page-content page-home">
    <!-- product -->
    <section class="store-new-products">
        <div class="container">
          <div class="row">
            <div class="col-12 aos-init aos-animate" data-aos="fade-up">
              <h5>Semua Products</h5>
            </div>
          </div>
		  <?php $setting = $this->db->get('settings')->row_array(); ?>
          <div class="row mt-3">
			<?php if($products->num_rows() > 0){ ?>
            <?php foreach($products->result_array() as $p): ?>
            <div class="col-6 col-md-4 col-lg-3 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
              <a class="component-products d-block" href="<?= base_url(); ?>p/<?= $p['slug']; ?>">
                <div class="products-thumbnail">
                  <div class="products-image" style="
                      background-image: url('<?= base_url(); ?>assets/images/product/<?= $p['img']; ?>');
                    "></div>
                </div>
                <div class="products-text">
                  <?= $p['title']; ?>
                </div>
                <div class="products-price">
                  	<div class="row">
						  <?php if($setting['promo'] == 1){ ?>
					<?php if($p['promo_price'] == 0){ ?>
								<div class="products-price newPrice text-right">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
						<?php }else{ ?>
								<div class="products-price oldPrice mb-0" style="color: <?= $this->config->item('default_color'); ?>">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
								<div class="products-price newPrice text-right">Rp <?= str_replace(",",".",number_format($p['promo_price'])); ?></div>
						<?php } ?>
						<?php }else{ ?>
								<div class="products-price newPrice text-right">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
					<?php } ?>
					  </div>
                </div>
              </a>
            </div>
            <?php endforeach; ?>
            <div class="clearfix"></div>
            <?php }else{ ?>
                <div class="alert alert-warning">Upss. Tidak ada produk yang dapat ditampilkan</div>
            <?php } ?>
          </div>
        </div>
      </section>
    </div>
    <!-- /end page -->
