<!-- content -->
    <div class="page-content page-cart">
      <section class="store-breadcrumbs" data-aos="fade-down" data-aos-delay="100">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <nav aria-label="">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="<?= base_url(); ?>">Home</a>
                  </li>
                   <li class="breadcrumb-item active">
                    Troli
                  </li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </section>

      <section class="store-cart">
        <div class="container">
          <div class="row" data-aos="fade-up" data-aos-delay="100">
            <div class="col-12 table-responsive">
              <table class="table table-borderless table-cart" aria-describedby="mydesc">
				  <?php if($this->cart->total_items() > 0){ ?>
                <thead>
                  <tr>
                    <th id="scop">Image</th>
                    <td>Name & Jumlah</td>
                    <td>Harga</td>
                    <td>Keterangan</td>
                    <td>Menu</td>
                  </tr>
                </thead>
                <tbody>
					<?php foreach($this->cart->contents() as $item): ?>
					<tr>
                    <td style="width:20%;">
                      <img src="<?= base_url(); ?>assets/images/product/<?= $item['img']; ?>" alt="" class="cart-image">
                    </td>
                    <td style="width:35%;">
                      <div class="product-title"><?= $item['name']; ?></div>
                      <div class="product-subtitle">Jumlah : <?= $item['qty']; ?></div>
                    </td>
                    <td style="width:15%;">
                      <div class="product-title">Rp <?= number_format($item['subtotal'],0,",","."); ?></div>
                    </td>
                    <td style="width:20%;">
						<small class="desc_product_<?= $item['rowid']; ?>"> <?= $item['ket']; ?> </small> 
                    </td>
                    <td style="width:40%;">
					<div class="row">
						<?php if($item['ket'] == ''){ ?>
							<span class="desc_product_<?= $item['rowid']; ?>"><a title="Tambah Catatan Tambahan" href="#" class="btn btn-deskripsi-cart" data-toggle="modal" data-target="#modalAddDescription" onclick="showModalAddKet('<?= $item['rowid']; ?>')"><i class="fas fa-plus-circle"></i> </a></span>
						<?php }else{ ?>
							<a href="#" title="Edit Catatan" class="btn btn-deskripsi-edit-cart" data-toggle="modal" data-target="#modalEditDescription" onclick="showModalEditKet('<?= $item['rowid']; ?>')"><i class="far fa-edit"></i></i></a>
						<?php } ?>
							<a href="<?= base_url(); ?>cart/delete/<?= $item['rowid']; ?>" onclick="return confirm('Yakin ingin menghapus produk ini dari troli?')" class="btn btn-remove-cart ml-1"><i class="fa fa-trash"></i> </a>
					</div>
                    </td>
				</tr>
					<?php endforeach; ?>
				<tr>
					<td>
						<a href="<?= base_url(); ?>cart/delete_cart" class="btn btn-outline-success mt-4 btn-block" onclick="return confirm('Apakah Anda yakin akan mengosongkan Troli?')">Kosongkan Troli</a>
					</td>
				</tr>
                <tr>
					<td>
						<?php }else{ ?>
							<div class="alert alert-warning">Upss. Troli masih kosong. Yuk belanja terlebih dahulu..</div>
						<?php } ?>
					</td>
				</tr>
                </tbody>
              </table>
            </div>
          </div>
         
          <div class="row" data-aos="fade-up" data-aos-delay="150">
            <div class="col-12">
              <hr/>
            </div>
            <div class="col-12 mb-4">
              <h2>
                Payment Informations
              </h2>
            </div>
          </div>
          <div class="row" data-aos="fade-up" data-aos-delay="200">
            <div class="col-4 col-md-2">
              <div class="product-title"><?= $this->cart->total_items(); ?></div>
              <div class="product-subtitle">Total Jumlah Barang</div>
            </div>
            <div class="col-4 col-md-3">
              <div class="product-title">Rp <?= number_format($this->cart->total(),0,",","."); ?></div>
              <div class="product-subtitle">Total Harga Barang</div>
            </div>
            <!-- <div class="col-4 col-md-2">
              <div class="product-title">$10</div>
              <div class="product-subtitle">Biaya Pengiriman</div>
            </div> -->
            <!-- <div class="col-4 col-md-2">
              <div class="product-title text-success">$10</div>
              <div class="product-subtitle">Total</div>
            </div> -->
            <div class="col-8 col-md-3">
			<?php if($this->cart->total_items() > 0){ ?>
				<a href="<?= base_url(); ?>payment"class="btn btn-success mt-4 btn-block">
				Lanjutkan
				</a>
			<?php }else{ ?>
				<a href="<?= base_url(); ?>"class="btn btn-success mt-4 btn-block">
				Belanja Dulu
				</a>
			 <?php } ?>
            </div>
          </div>
        </div>
      </section>
    </div>

	<!-- Modal -->
<div class="modal fade" id="modalEditDescription" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Deskripsi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="bodyModalEditKet">
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btnEditKetProduct" data-dismiss="modal">Simpan</button>
        </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalAddDescription" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Deskripsi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="bodyModalAddKet">
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btnSaveKetProduct" data-dismiss="modal">Simpan</button>
        </div>
        </div>
    </div>
</div>

  <!-- end content -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script>

    function showModalAddKet(id){
        $("#bodyModalAddKet").html(`<div class="form-group">
            <textarea name="ket_product" id="ket_product" class="form-control form-control-sm" placeholder="Model, ukuran, warna, dll."></textarea>
            <input type="hidden" id="rowid_pro" value=${id}>
        </div>`);
    }

    function showModalEditKet(id){
        $.ajax({
            url: "<?= base_url(); ?>cart/get_item",
            type: "post",
            dataType: "json",
            data: {
                rowid: id
            },
            success: function(res){
                $("#bodyModalEditKet").html(`<div class="form-group">
                    <textarea name="ket_product" id="ket_product_edit" class="form-control form-control-sm" placeholder="Model, ukuran, warna, dll.">${res.ket}</textarea>
                    <input type="hidden" id="rowid_pro_edit" value=${id}>
                </div>`);      
            }
        })
    }

    $("#btnSaveKetProduct").on('click', function(){
        const rowid = $("#rowid_pro").val();
        const ket = $("#ket_product").val();
        $.ajax({
            url: "<?= base_url(); ?>cart/add_ket",
            type: "post",
            data: {
                rowid: rowid,
                ket: ket
            },
            success: function(){
                $("small.desc_product_"+rowid).html( ket );
                $("span.desc_product_"+rowid).html(`<a href="#" title="Edit Catatan" class="btn btn-deskripsi-edit-cart" data-toggle="modal" data-target="#modalEditDescription" onclick="showModalEditKet('<?= $item['rowid']; ?>')"><i class="far fa-edit"></i>`);
            }
        })
    })

    $("#btnEditKetProduct").on('click', function(){
        const rowid = $("#rowid_pro_edit").val();
        const ket = $("#ket_product_edit").val();
        $.ajax({
            url: "<?= base_url(); ?>cart/add_ket",
            type: "post",
            data: {
                rowid: rowid,
                ket: ket
            },
            success: function(){
                $("small.desc_product_"+rowid).html( ket );
				
            }
        })
    })

</script>
