
      <!-- categories -->
      <section class="store-trend-categories">
        <div class="container">
          <div class="row">
						<?php foreach($categoriesLimit->result_array() as $c): ?>
            <div class="col-6 col-md-3 col-lg-2 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
              <a class="component-categories d-block" href="<?= base_url(); ?>c/<?= $c['slug']; ?>">
                <div class="categories-image">
                  <img
                    src="<?= base_url(); ?>assets/images/icon/<?= $c['icon']; ?>"
                    alt="Gadgets Categories"
                    class="w-100"
                  />
                </div>
                <p class="categories-text"><?= $c['name']; ?></p>
              </a>
            </div>
						<?php endforeach; ?>
            <div class="col-6 col-md-3 col-lg-2 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
              <a class="component-categories d-block" data-toggle="modal" data-target="#modalMoreCategory"">
                <div class="categories-image">
                  <img
                    src="<?= base_url(); ?>assets/images/icon/category-more.svg"
                    alt="Gadgets Categories"
                    class="w-100"
                  />
                </div>
                <p class="categories-text">Lainnya</p>
              </a>
            </div>
          </div>
        </div>
      </section>
			<!-- Modal More Category -->
		<div class="modal fade" id="modalMoreCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalCenterTitle">Semua Kategori</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="main-category col-12">
							<?php foreach($categories->result_array() as $c): ?>
								<div class="col-3">
									<a class="component-categories d-block" href="<?= base_url(); ?>c/<?= $c['slug']; ?>">
										<div class="categories-image">
											<img
												src="<?= base_url(); ?>assets/images/icon/<?= $c['icon']; ?>"
												alt="Gadgets Categories"
												class="w-100"
											/>
										</div>
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>

      <!-- promo -->
			<?php if($promo->num_rows() > 0){ ?>
			<?php if($setting['promo'] == 1){ ?>
      <section class="store-new-products">
        <div class="container">
          <div class="row">
            <div class="col-12 mb-4 aos-init aos-animate" data-aos="fade-up">
              <h5>Promo</h5>
							<small class="lead"><i class="fa fa-fire-alt"></i> Berakhir dalam <span id="countdownPromo"></span></small>
							<a href="<?= base_url(); ?>promo"><span class="float-right"  style="color: <?= $this->config->item('default_color'); ?>"><i class="fa fa-shopping-basket"></i> Lihat Semua</span></a>
            </div>
          </div>
          <div class="row">
						<?php foreach($getPromo->result_array() as $data): ?>
            <div
              class="col-6 col-md-4 col-lg-3 aos-init aos-animate"
              data-aos="fade-up"
              data-aos-delay="100"
            >
              <a class="component-products d-block" href="<?= base_url(); ?>p/<?= $data['slug']; ?>">
                <div class="products-thumbnail">
                  <div
                    class="products-image"
                    style="
                      background-image: url('<?= base_url(); ?>assets/images/product/<?= $data['img'] ?>');
                    "
                  ></div>
                </div>
                <div class="products-text"><?= $data['title'] ?></div>
                <div class="row">
									<div class="products-price oldPrice" style="color: <?= $this->config->item('default_color'); ?>">Rp.<?= str_replace(".",",",number_format($data['price'])) ?></div>
                	<div class="products-price newPrice text-right" >Rp <?= str_replace(".",",",number_format($data['promo_price'])) ?></div>
								</div>
              </a>
            </div>
            <?php endforeach; ?>
          </div>
        </div>
      </section>
			<?php }else{ ?>
				<br><br>
			<?php } ?>
			<?php }else{ ?>
				<br><br>
			<?php } ?>


			<!-- Produk terlaris -->
			<?php if($best->num_rows() > 0){ ?>
      <section class="store-new-products">
        <div class="container">
          <div class="row">
            <div class="col-12 aos-init aos-animate" data-aos="fade-up">
              <h5>Products Terlaris</h5>
            </div>
          </div>
          <div class="row">
						<?php foreach($best->result_array() as $p): ?>
            <div class="col-6 col-md-4 col-lg-3 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
              <a class="component-products d-block" href="<?= base_url(); ?>p/<?= $p['slug']; ?>">
                <div class="products-thumbnail">
                  <div
                    class="products-image"
                    style="
                      background-image: url('<?= base_url(); ?>assets/images/product/<?= $p['img']; ?>');
                    "
                  ></div>
                </div>
                <div class="products-text"><?= $p['title']; ?></div>
								<div class="row">
									<?php if($setting['promo'] == 1){ ?>
									<?php if($p['promo_price'] == 0){ ?>
												<div class="products-price newPrice">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
										<?php }else{ ?>
												<div class="products-price oldPrice mb-0" style="color: <?= $this->config->item('default_color'); ?>">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
												<div class="products-price newPrice text-right">Rp <?= str_replace(",",".",number_format($p['promo_price'])); ?></div>
										<?php } ?>
										<?php }else{ ?>
												<div class="products-price newPrice text-right">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
										<?php } ?>
								</div>
              </a>
            </div>
            <?php endforeach; ?>
          </div>
        </div>
      </section>
			<?php } ?>
    </div>
    <!-- /end page -->

			<!-- Produk Terbaru -->
      <section class="store-new-products">
        <div class="container">
          <div class="row">
            <div class="col-12 aos-init aos-animate" data-aos="fade-up">
              <h5>Produk terbaru</h5>
							<a href="<?= base_url(); ?>products"><span class="float-right"  style="color: <?= $this->config->item('default_color'); ?>"><i class="fa fa-shopping-basket"></i> Lihat Semua</span></a>
							<img src="" class="banner-package" alt="banner" style="width: 100%; opacity: 0; object-fit: cover">
            </div>
          </div>
          <div class="row">
						<?php foreach($recent->result_array() as $p): ?>
            <div
              class="col-6 col-md-4 col-lg-3 aos-init aos-animate"
              data-aos="fade-up"
              data-aos-delay="100"
            >
              <a class="component-products d-block" href="<?= base_url(); ?>p/<?= $p['slug']; ?>">
                <div class="products-thumbnail">
                  <div
                    class="products-image"
                    style="
                      background-image: url('<?= base_url(); ?>assets/images/product/<?= $p['img']; ?>');
                    "
                  ></div>
                </div>
								<div class="row">
									<?php if($setting['promo'] == 1){ ?>
									<?php if($p['promo_price'] == 0){ ?>
												<div class="products-text"><?= $p['title']; ?></div>
												<div class="products-price newPrice text-right">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
										<?php }else{ ?>
												<div class="products-text"><?= $p['title']; ?></div>
												<div class="products-price oldPrice mb-0" style="color: <?= $this->config->item('default_color'); ?>">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
												<div class="products-price newPrice text-right">Rp <?= str_replace(",",".",number_format($p['promo_price'])); ?></div>
										<?php } ?>
										<?php }else{ ?>
												<div class="products-text"><?= $p['title']; ?></div>
												<div class="products-price newPrice text-right">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
										<?php } ?>
								</div>
              </a>
            </div>
            <?php endforeach; ?>
          </div>
        </div>
      </section>
    </div>
    <!-- /end page -->

